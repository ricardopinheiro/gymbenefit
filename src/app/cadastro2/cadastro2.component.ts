import { Tipo } from './../tipo';
import { Banco } from './../bancos';
import { Infra } from './../infra';
import { Categoria } from './../categoria';
import { Modalidade } from './modalidade';

import { Component, OnInit, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { CropperSettings, ImageCropperComponent } from 'ng2-img-cropper';

import { CadastroService } from './../cadastro.service';

@Component({
  selector: 'app-cadastro2',
  templateUrl: './cadastro2.component.html',
  styleUrls: ['./cadastro2.component.css', '../app.component.css']
})
export class Cadastro2Component implements OnInit {
  



  @ViewChild('show') show: boolean;
  
  modalidades:Modalidade[];
  modalidade:Modalidade[];
  modalidadesSelecionadas:string[];
  categoriasSelecionadas:string[];
  categorias:Categoria[];
  infra:Infra[];
  checaArray:any;
  tipos:Tipo[]  
  data: any;
  cropperSettings: CropperSettings; 

  constructor(private cadastroService:CadastroService) { 

	this.cropperSettings = new CropperSettings();
	this.cropperSettings.width = 200;
	this.cropperSettings.height = 200;
	this.cropperSettings.croppedWidth = 200;
	this.cropperSettings.croppedHeight = 200;
	this.cropperSettings.canvasWidth = 400;
	this.cropperSettings.canvasHeight = 400;
	this.cropperSettings.dynamicSizing = false;
	this.cropperSettings.minHeight = 400;
	this.cropperSettings.minWidth = 400;

	this.data = {};
   }
   getModalidade(){
		this.cadastroService.getModalidades()
					.then(modalidaes => this.modalidades = modalidaes);
	}
   getCategoria(){
		this.cadastroService.getCategorias()
					.then(categorias => this.categorias = categorias);
	}
	getInfra(){
		this.cadastroService.getInfra()
					.then(infra => this.infra = infra)
		
	}
	getTipo(){
		this.cadastroService.getTipos()
					.then(tipos => this.tipos = tipos)
		
	}
	postModalidades(){
		this.cadastroService.postModalidades(this.modalidadesSelecionadas);
	}
   
  ngOnInit() {
	this.getModalidade();
	this.getCategoria();
	this.getInfra();
	this.getTipo();
	// this.modalidades = this.cadastroService.getModalidades();
	// console.log(this.modalidades)
	// this.categorias = this.cadastroService.getCategoria();
	this.modalidadesSelecionadas = this.cadastroService.getModalidadesSelecionadas();
	this.categoriasSelecionadas = this.cadastroService.getCategoriasSelecionadas();
  }
  ngOnChanges(changes: SimpleChanges): void {
	this.modalidadesSelecionadas = this.cadastroService.getModalidadesSelecionadas();
	this.categoriasSelecionadas = this.cadastroService.getCategoriasSelecionadas();
  }
  onRemoveModalidade(modalidade:string){
	this.cadastroService.removeModalidade(modalidade);
	console.log(this.cadastroService.getModalidadesSelecionadas());
  }
  onRemoveCategoria(categoria:string){
	this.cadastroService.removeCategoria(categoria);
	console.log(this.cadastroService.getCategoriasSelecionadas());
  }
  onAddModalidade(modalidade:string){
	this.checaArray = this.cadastroService.getModalidadesSelecionadas().indexOf(modalidade);
	if (this.checaArray == -1) {
	  this.cadastroService.addModalidade(modalidade);
	  console.log(this.cadastroService.getModalidadesSelecionadas());
	}
  }
  onAddCategoria(categoria:string){
	this.checaArray = 0;
	this.checaArray = this.cadastroService.getCategoriasSelecionadas().indexOf(categoria);
	if (this.checaArray == -1) {
	  this.cadastroService.addCategoria(categoria);
	  console.log(this.cadastroService.getCategoriasSelecionadas());
	}
  }
  
  
}
