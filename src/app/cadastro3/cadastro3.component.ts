import { Modalidade } from './../cadastro2/modalidade';
import { Tipo } from './../tipo';
import { CadastroService } from './../cadastro.service';
import { Plano } from './../plano';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-cadastro3',
  templateUrl: './cadastro3.component.html',
  styleUrls: ['./cadastro3.component.css', '../app.component.css']
})
export class Cadastro3Component implements OnInit {

  constructor( private formBuilder : FormBuilder,
				private cadastroService: CadastroService) { }


  formModalidade:FormGroup
  formUnico:FormGroup
  formDiferentes:FormGroup
  control:string
  planos:Plano[] = [];
  model;
  tipos:Tipo[];
  modalidades:Modalidade[];

	ngOnInit() {

		this.formModalidade = this.formBuilder.group({
		modalidade: ''
		})
		this.formUnico = this.formBuilder.group({
		ValorMensal: [null, Validators.required],
		ValorVisita: [null, Validators.required]
		})
		this.formDiferentes = this.formBuilder.group({
			ValorMensal: [null, Validators.required],
			ValorVisita: [null, Validators.required],
			tipoPlanoId: [null, Validators.required],
			Modalidades: [null]
		})
		this.getTipo();
		this.getModalidades()
	}
	ngOnChanges(changes: SimpleChanges) {
		
	}
  
	addPlano(plano){
		this.model = plano
		console.log(this.model)
		this.planos.push(this.model)
		console.log(this.planos)
		this.formDiferentes.reset();
	}
	deletaPlano(id){
		
		this.planos.splice(id,1);
		
 	}
	editaPlano(){
		console.log(this.planos[0])
	}
	encontraPlano(id){
		return this.planos.find(x => this.model.tipoPlanoId.value==id)
	}
	getTipo(){
		this.cadastroService.getTipos()
					.then(tipos => this.tipos = tipos)
		
	}
	getModalidades(){
		this.cadastroService.getListaModalidade()
					.then(modalidades => this.modalidades = modalidades)
	}
}
