import { Http } from '@angular/http';
import { FormsModule, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ViewChild, SimpleChanges } 									from '@angular/core';
import { Component, OnInit } 											from '@angular/core';

import 'rxjs/Rx'
import {Observable} from 'rxjs/Rx';

import { CpfCnpjDirective } 	from './../cpfcnpj.directive';
import { CpfCnpjPipe } 			from './../cnpjcpf.pipe';
import { Banco } 				from './../bancos';
import { CadastroService }		from './../cadastro.service';
import { Cadastro } 			from './../cadastro';



@Component({
	selector: 'app-cadastro',
	templateUrl: './cadastro.component.html',
	styleUrls: ['./cadastro.component.css', '../app.component.css']
})
export class CadastroComponent implements OnInit {
	


	constructor(private cadastroService:CadastroService,
				private formBuilder: FormBuilder,
				private http:Http) { }
	formCadastro : FormGroup; 
	bancos:Banco[];
	
	ngOnInit() {
		this.getBanco();

		


		this.formCadastro = this.formBuilder.group({
			NomeFantasia: [ '', [Validators.required, Validators.maxLength(80)] ],
			RazaoSocial: [ null , [Validators.required, Validators.maxLength(80)] ],
			CNPJ: [ null, [Validators.required, Validators.maxLength(18), Validators.minLength(18)] ],
			Telefone: [ null, [Validators.required, Validators.maxLength(20)] ],
			Site: [ null, [Validators.maxLength(80)] ],
			NomeCompleto: [ null, [Validators.required, Validators.maxLength(80)] ],
			Cargo: [ null, [Validators.required, Validators.maxLength(80)] ],
			TelefoneFixo: [ null, [Validators.required, Validators.maxLength(20)] ],
			Celular: [ null, [Validators.required, Validators.maxLength(20)] ],
			Email: [ null, [Validators.required, Validators.email, Validators.maxLength(80)]],
			CEP: [ null, [Validators.required, Validators.maxLength(9)] ],
			Endereco: [ null, [Validators.required, Validators.maxLength(80)] ],
			Numero: [ null, Validators.required ],
			Complemento: [ null, Validators.maxLength(80)],
			Bairro: [ null, [Validators.required, Validators.maxLength(80)] ],
			Cidade: [ null, [Validators.required, Validators.maxLength(80)] ],
			UF: [ null, [Validators.required, Validators.maxLength(80)] ],
			Pais: [ null, [Validators.required, Validators.maxLength(80)] ],
			Banco: [ 1, Validators.required ],
			Agencia: [ null, [Validators.required, Validators.maxLength(10)] ],
			Conta: [ null, [Validators.required, Validators.maxLength(20)] ],
			ContaDV: [ null, [Validators.required, Validators.maxLength(2)] ]
		});

	}
	ngOnChanges(changes: SimpleChanges) {
		

		console.log(this.formCadastro.valid);
		console.log(this.formCadastro.value);
		
	}

	
	onSubmit(){
		console.log(this.formCadastro.value);
		this.cadastroService.postCadastro(this.formCadastro);
		this.formCadastro.reset();
	}

	verificaValid(campo){
		return !this.formCadastro.get(campo).valid && this.formCadastro.get(campo).touched;

	}
	aplicaCssErro(campo){
		return{
			'has-error': this.verificaValid(campo),
		}
	}
	getBanco(){
		this.cadastroService.getBancos()
			.then( bancos => this.bancos = bancos)
	}
	// replaceCnpj(){
	// 	CNPJ.value.replace(value,/\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}/)
	// }
	consultaCep(cep){
		cep = cep.replace(/\D/g,'');

		if(cep != ""){
			var validacep = /^[0-9]{8}/;
			if (validacep.test(cep)) {

				this.http.get(`//viacep.com.br/ws/${cep}/json/`)
				.map(res => res.json())
				.subscribe(res => this.populaCadastro(res, this.formCadastro));
			
				
			}
		}
	}
	populaCadastro(res, form){
		
		form.patchValue({

			CEP: res.cep,
			Endereco: res.logradouro,
			Bairro: res.bairro,
			Cidade: res.localidade,
			UF: res.uf,
			Pais: 'Brasil'
			
		})
	}
}
