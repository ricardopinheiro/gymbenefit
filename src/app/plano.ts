import { Modalidade } from './cadastro2/modalidade';
export class Plano{
    constructor(
        public mensalidade:string,
        public valorVisita:string,
        public tipoPlanoId:number,
        public modalidades:Modalidade[]
    ){}
}