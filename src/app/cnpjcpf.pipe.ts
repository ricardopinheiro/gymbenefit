import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
	name: 'cpfCnpj'
})
@Injectable()
export class CpfCnpjPipe implements PipeTransform{
	transform(value: string) {
		if(value.length == 14){
			let a = value.substring(0, 2);
			let b = value.substring(2, 5);
			let c = value.substring(5, 8);
			let d = value.substring(8, 12);
			let e = value.substring(12, 14);
			return a + '.' + b + '.' + c + '/' + d + '-'+ e;
		}
	}
}