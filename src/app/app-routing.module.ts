import { ContratoComponent } from './contrato/contrato.component';
import { ModuleWithProviders }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CadastroComponent }   from './cadastro/cadastro.component';
import { AppComponent } from './app.component';
import { GymbenefitHomeComponent } from './gymbenefit-home/gymbenefit-home.component';
import { Cadastro2Component } from './cadastro2/cadastro2.component';
import { Cadastro4Component } from './cadastro4/cadastro4.component';
import { Cadastro3Component } from './cadastro3/cadastro3.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path:'cadastro', component: CadastroComponent },
    { path:'home', component: GymbenefitHomeComponent },
    { path: 'cadastro-2', component: Cadastro2Component },
    { path: 'cadastro-3', component: Cadastro3Component },
    { path: 'cadastro-4', component: Cadastro4Component },
    { path: 'contrato', component: ContratoComponent }
];

export const routing : ModuleWithProviders = RouterModule.forRoot(routes);
