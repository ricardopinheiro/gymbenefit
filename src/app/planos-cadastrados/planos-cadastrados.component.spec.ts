import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanosCadastradosComponent } from './planos-cadastrados.component';

describe('PlanosCadastradosComponent', () => {
  let component: PlanosCadastradosComponent;
  let fixture: ComponentFixture<PlanosCadastradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanosCadastradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanosCadastradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
