import { Tipo } from './tipo';
import { CadastroComponent } from './cadastro/cadastro.component';
import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import 'rxjs/Rx'
import { Observable } from 'rxjs/Rx';

import { Infra } from './infra';
import { Banco } from './bancos';
import { Modalidade } from './cadastro2/modalidade';
import { Categoria } from './categoria';


@Injectable()

export class CadastroService {
	checkArray:any;
	modalidadesSelecionadas:string[] = [];
	categoriasSelecionadas:string[] = [];

	 constructor( public http:Http	) { 
	
  }


//   getCategoria(){
// 	return this.categorias;
//   }
  
  getModalidadesSelecionadas(){
	  return this.modalidadesSelecionadas;
  }
  getCategoriasSelecionadas(){
	  return this.categoriasSelecionadas;
  }
  removeModalidade(categoria:string){
	this.checkArray = this.modalidadesSelecionadas.indexOf(categoria);
	console.log(this.checkArray)
	if (this.checkArray !== -1){
	  this.modalidadesSelecionadas.splice(this.checkArray, 1);
	}
  }
  removeCategoria(modalidade:string){
	this.checkArray = this.categoriasSelecionadas.indexOf(modalidade);
	console.log(this.checkArray)
	if (this.checkArray !== -1){
	  this.categoriasSelecionadas.splice(this.checkArray, 1);
	}
  }
  addModalidade(modalidade:string){
	this.modalidadesSelecionadas.push(modalidade);
  }
  addCategoria(categoria:string){
	this.categoriasSelecionadas.push(categoria);
  }
  postCadastro(formCadastro){
	var headers = new Headers();

	headers.append('Content-Type', 'application/json');


	  return this.http.post('http://192.168.100.10:29451/api/Cadastro/Inicio', JSON.stringify(formCadastro.value), { headers: headers })
		.map(res => res.json())
		.subscribe( dados => console.log(dados));
	}
	postModalidades(modalidades){
		var headers = new Headers();
		
		headers.append('Content-Type', 'application/json');
	
	
		return this.http.post('http://192.168.100.10:29451/api/Cadastro/Modalidade/f969a54c-54ba-4491-81bb-f583f28bb8fa', JSON.stringify(modalidades.value), { headers: headers })
		.map(res => res.json())
		.subscribe( dados => console.log(dados));
	}


	getBancos(): Promise<Banco[]>{
		return this.http.get('http://192.168.100.10:29451/api/Banco')
		.toPromise()
		.then(response => response.json().data as Banco[]);
	}
	getModalidades(): Promise<Modalidade[]>{
		
		return this.http.get('http://192.168.100.10:29451/api/Modalidade')
		.toPromise()
		.then(response => response.json().data as Modalidade[]);
	}

	getCategorias(): Promise<Categoria[]>{
		
		return this.http.get('http://192.168.100.10:29451/api/Categoria')
		.toPromise()
		.then( response => response.json().data as Categoria[]);
	}

	getInfra(): Promise<Infra[]>{
		return this.http.get('http://192.168.100.10:29451/api/Infraestrutura')
		.toPromise()
		.then( response => response.json().data as Infra[]);
	}
	getTipos(): Promise<Tipo[]>{
		return this.http.get('http://192.168.100.10:29451/api/TipoPlano')
		.toPromise()
		.then( response => response.json().data as Tipo[]);
	}
	getListaModalidade():Promise<Modalidade[]>{

		return this.http.get('http://192.168.100.10/api/Cadastro/Modalidade/729dd898-7a92-40b4-bc58-1430de851b31')
		.toPromise()
		.then( response => response.json().data as Modalidade[]);
		
	}
}
