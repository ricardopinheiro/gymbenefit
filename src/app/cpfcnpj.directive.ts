import { ElementRef, HostListener, Directive, Input } from "@angular/core";
import { NgControl } from "@angular/forms";

@Directive({
  selector: '[ngModel][cpfcnpj]'
})
export class CpfCnpjDirective {
  constructor(private el: ElementRef, public model: NgControl) {}

  @HostListener('input',['$event']) onEvent($event:any){
    var digit = this.el.nativeElement.value.replace(/\D/g, '');
    let result01:any = 0;
    let result02:any = 0;
    let cpf:boolean;
    let cnpj:boolean;

      if(digit.length >= 14) {
        let cont = digit.length - 2;
        let position = cont - 7;
        for(let i=cont; i>=1; i--){
          result01 += digit.charAt(cont - i) * position--;
          if (position < 2) {
            position = 9;
          }
        }

        cont = digit.length - 1;
        position = cont - 7;
        for(let i=cont; i>=1; i--){
          result02 += digit.charAt(cont - i) * position--;
          if (position < 2) {
            position = 9;
          }
        }
        let num01 = ((result01%11) < 2) ? 0 : 11-(result01%11);
        let num02 = ((result02%11) < 2) ? 0 : 11-(result02%11);

        if ((num01 != digit[12]) || (num02 != digit[13])){
          cnpj = false;
        } else {
          cnpj = true;
        }

        digit = digit.substring(0, 2) + '.' + digit.substring(2, 5) + '.' + digit.substring(5,8) + '/' + digit.substring(8,12) + '-' + digit.substring(12,14);
      }
    

    this.model.control.setValue(digit);
  }
}