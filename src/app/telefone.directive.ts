import { ElementRef, HostListener, Directive, Input } from "@angular/core";
import { NgControl } from "@angular/forms";

@Directive({
selector: '[ngModel][appTelefone]'
})
export class TelefoneDirective {

	constructor(private el: ElementRef, public model: NgControl) { }
	@HostListener('input',['$event']) onEvent($event:any){
		var digit = this.el.nativeElement.value.replace(/\D/g,'');
		let resultDDD:any = 0;
		let result1:any = 0;
		let result2:any = 0;

		let telefone:boolean;
		let ceular:boolean;


		if(digit.length == 11 ){
			digit = '(' + digit.substring(0,2) + ')' + digit.substring(2,7) + '-' + digit.substring(7,12);
		}
		
		if(digit.length == 10 ){
			digit = '(' + digit.substring(0,2) + ')' + digit.substring(2,6) + '-' + digit.substring(6,11);
		}
		if(digit.length == 8){
			digit = digit.substring(0,4) + '-' + digit.substring(4,8);
		}
		if( digit.length == 9 && digit[4] != '-' ){
			digit = digit.substring(0,5) + '-' + digit.substring(5,9);
		}
		
		this.model.control.setValue(digit);
	}
}

