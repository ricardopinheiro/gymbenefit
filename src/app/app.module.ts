import { CpfCnpjDirective } from './cpfcnpj.directive';
import { CpfCnpjPipe } from './cnpjcpf.pipe';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule }        	from '@angular/platform-browser';
import { NgModule } 				from '@angular/core';
import { RouterModule, Routes } 	from '@angular/router';

import { ImageCropperComponent } from 'ng2-img-cropper';


import { CadastroService } from './cadastro.service';
import { AppComponent } 			from './app.component';
import { CadastroComponent } 		from './cadastro/cadastro.component';
import { GymbenefitHomeComponent } 	from './gymbenefit-home/gymbenefit-home.component';
import { routing } 					from './app-routing.module';
import { Cadastro2Component } from './cadastro2/cadastro2.component';
import { Cadastro3Component } from './cadastro3/cadastro3.component';
import { Cadastro4Component } from './cadastro4/cadastro4.component';
import { PlanosCadastradosComponent } from './planos-cadastrados/planos-cadastrados.component';
import { ContratoComponent } from './contrato/contrato.component';
import { TelefoneDirective } from './telefone.directive';


@NgModule({
  declarations: [
    AppComponent,
    CadastroComponent,
    GymbenefitHomeComponent,
    Cadastro2Component,
    Cadastro3Component,
    Cadastro4Component,
    ImageCropperComponent,
    PlanosCadastradosComponent,
    ContratoComponent,
    CpfCnpjPipe,
    CpfCnpjDirective,
    TelefoneDirective
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    routing,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    
  ],
  exports: [CpfCnpjPipe],
  providers: [CadastroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
