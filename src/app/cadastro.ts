export class Cadastro{
    
    constructor(
        public  NomeFantasia:string,
        public  RazaoSocial:string,
        public  CNPJ:string,
        public  Telefone:string,
        public  Site:string,
        public  NomeCompleto:string,
        public  Cargo:string,
        public  TelefoneFixo:string,
        public  Celular:string,
        public  Email:string,
        public  CEP:string,
        public  Endereco:string,
        public  Numero:string,
        public  Complemento:string,
        public  Bairro:string,
        public  Cidade:string,
        public  UF:string,
        public  Pais:string,
        public  Banco:number,
        public  Agencia:string,
        public  Conta:string,
        public  ContaDV:string
    ){}

}