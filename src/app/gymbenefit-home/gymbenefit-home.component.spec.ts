import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymbenefitHomeComponent } from './gymbenefit-home.component';

describe('GymbenefitHomeComponent', () => {
  let component: GymbenefitHomeComponent;
  let fixture: ComponentFixture<GymbenefitHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymbenefitHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymbenefitHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
